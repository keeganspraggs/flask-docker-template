FROM python:3.5

ENV APPLICATION_ROOT /usr/src/app


RUN mkdir -p $APPLICATION_ROOT
WORKDIR $APPLICATION_ROOT
ADD requirements.txt $APPLICATION_ROOT
RUN pip install -r requirements.txt
