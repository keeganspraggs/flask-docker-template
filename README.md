Simple cookiecutter style auto-reloading  flask app using docker as the dev enviroment.

## Usage:

* Clone
* rename (to your project name) and cd into directory
* run "docker-compose build"
* run "docker-compose up -d" to run dettached
* go to http://localhost:8080 to test it out


## Config:

### app.py
The main file for your app. Entry point for the enviroment.

### gunicorn.py
Contains the configs for Gunicorn. Change parameters in there to change the docker images gunicorn command parameters.
